# HydrationAnalysis

Updated by Jesse

Usage:

1. take photos of urine of hydrated/dehydrated
2. create a folder named as UrinePhotos
3. create two folders named hydrated and dehydrated inside UrinePhotos folder
4. copy photos to UrinePhotos\hydrated and UrinePhotos\dehydrated respectively
5. open visual studio to build PhotoAnalyzer project
6. double click PhotoAnalyzer.reg inside C:\hydrationanalysis\PhotoAnalyzer\PhotoAnalyzer folder
7. open C:\hydrationanalysis\UrinePhotos folder
8. right mouse click inside the folder to choose PhotoAnalysis menu item
9. Once the program finishes, two files named hydrated.csv and dehydrated.csv will be created inside C:\hydrationanalysis\UrinePhotos folder