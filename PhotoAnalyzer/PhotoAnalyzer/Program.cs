﻿using StatisticsLibrary;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhotoAnalyzer
{
    class Program
    {
        static void Main(string[] args)
        {
            string cwd = Directory.GetCurrentDirectory();
            DirectoryInfo dirInfo = new DirectoryInfo(cwd);
            IEnumerable<DirectoryInfo> folders = dirInfo.GetDirectories();

            foreach(DirectoryInfo di in folders)
            {
                string csvFile = Path.Combine(cwd, di.Name + ".csv");
                if (File.Exists(csvFile))
                {
                    File.Delete(csvFile);
                }
                IEnumerable<FileInfo> imageFiles = di.GetFiles("*.jpg");
                foreach(FileInfo fi in imageFiles)
                {
                    AnalyzePhoto(fi.FullName, csvFile);
                }
            }
        }

        private static void AnalyzePhoto(string imageFile, string csvFile)
        {
            Bitmap bmp = Load(imageFile);
            AnalyzeRGB(ref bmp, csvFile);
        }

        private static Bitmap Load(string fileName)
        {
            Image image = Image.FromFile(fileName);
            return new Bitmap(image);
        }

        private static void AnalyzeRGB(ref Bitmap bmp, string labelFile)
        {
            double[] red = new double[bmp.Height * bmp.Width];
            double[] green = new double[bmp.Height * bmp.Width];
            double[] blue = new double[bmp.Height * bmp.Width];

            for (int r = 0; r < bmp.Height; r++)
            {
                for (int c = 0; c < bmp.Width; c++)
                {
                    Color rgb = bmp.GetPixel(c, r);
                    red[r*bmp.Width + c] = rgb.R;
                    green[r*bmp.Width + c] = rgb.G;
                    blue[r*bmp.Width + c] = rgb.B;
                }
            }
            SampleStatistics sampleStatistics = new SampleStatistics();
            double redMean = sampleStatistics.Mean(red);
            double redStd = sampleStatistics.StandardDeviation(red);
            double greenMean = sampleStatistics.Mean(green);
            double greenStd = sampleStatistics.StandardDeviation(green);
            double blueMean = sampleStatistics.Mean(blue);
            double blueStd = sampleStatistics.StandardDeviation(blue);

            if (File.Exists(labelFile) == false)
            {
                //File.WriteAllLines(labelFile, new string[] { "redMean,redStd,greenMean,greenStd,blueMean,blueStd" });
                File.WriteAllLines(labelFile, new string[] { "redMean,greenMean,blueMean" });
            }
            //File.AppendAllLines(labelFile, new string[] { redMean.ToString() + "," + redStd.ToString() + "," + greenMean.ToString() + "," + greenStd.ToString() + "," + blueMean.ToString() + "," + blueStd.ToString() });
            File.AppendAllLines(labelFile, new string[] { redMean.ToString() + "," + greenMean.ToString() + "," + blueMean.ToString() });
        }
    }
}
